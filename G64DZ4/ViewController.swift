//
//  ViewController.swift
//  G64DZ4
//
//  Created by mac on 7/17/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let name = "Тарас"
        let otchestvo = "Алексеевич"
        let firstLastName: NSMutableString = "ТарасФилатов"
        let numbers1: NSMutableString = "1234567"
        let numbers2: NSMutableString = "12345"
        //let password = "hB37va>8e"
        zadanie1(name: (name)) //Задача 1. Создать строку с своим именем, вывести количество символов содержащихся в ней.
        zadanie2(otchestvo: (otchestvo)) //Задача 2. Создать строку с своим отчеством проверить его на окончание “ич/на”
        zadanie3(firstLastName: (firstLastName)) //Задача 3. Cоздать строку, где слитно написано Ваши ИмяФамилия
        zadanie4(word: "Ось") //Задача 4. Вывести строку зеркально Ось → ьсО не используя reverse (посимвольно)
        zadanie5(numbers1: (numbers1), numbers2: (numbers2))
        //zadanie6(password: (password))
        
        
    }
    
    //Задача 1. Создать строку с своим именем, вывести количество символов содержащихся в ней.
    func zadanie1(name: String) {
        print("Имя \(name) содержит \(name.count) символов")
    }
    
    //Задача 2. Создать строку с своим отчеством проверить его на окончание “ич/на”
    func zadanie2(otchestvo: String) {
        if otchestvo.hasSuffix("ич") {
            print("В отчестве \(otchestvo) есть окончание \"ич\" ")
        }
        else {
            print("В отчестве \(otchestvo) нет окончания \"ич\" ")
        }
    }
    
    //Задача 3. Cоздать строку, где слитно написано Ваши ИмяФамилия “IvanVasilevich"
    func zadanie3(firstLastName: NSMutableString) {
        let firstName = firstLastName
        let range = NSRange(location: 0, length: 5)
        print(firstName.substring(with: range))
        
        let lastName = firstLastName
        let range1 = NSRange(location: 5, length: 7)
        //firstLastName.deleteCharacters(in: range)
        print(lastName.substring(with: range1))
        
        let firstLastName = firstLastName
        firstLastName.insert(" ", at: 5)
        print(firstLastName)
    }
    
    //Задача 4. Вывести строку зеркально Ось → ьсО не используя reverse (посимвольно)
    func zadanie4(word: String) {
        let symbol1 = word[word.startIndex]
        let symbol2 = word[word.index(after: word.startIndex)]
        let symbol3 = word[word.index(before: word.endIndex)]
        print(symbol1, symbol2, symbol3, separator: "")
        print(symbol3, symbol2, symbol1, separator: "")
    }
    
    //Задача 5. Добавить запятые в строку как их расставляет калькулятор. 1234567 → 1,234,567. 12345 → 12,345
    func zadanie5(numbers1: NSMutableString, numbers2: NSMutableString) {
        let numbers1 = numbers1
        numbers1.insert(",", at: 1)
        numbers1.insert(",", at: 5)
        print(numbers1)
        let numbers2 = numbers2
        numbers2.insert(",", at: 2)
        print(numbers2)
    }
    
    //    //Задача 6. Проверить пароль на надежность от 1 до 5
    //    func zadanie6(password: String) {
    //        let password = password
    
}



